﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arcable : MonoBehaviour {

    [Header("Collection Arc Settings")]
    public float arcFraction = 0.5f;
    public float arcTime = 1.0f;
    protected bool travelling = false;

    public bool Travelling {
        get { return travelling; }
    }

    public IEnumerator ArcToPlayer(PlayerBodyController body) {
        travelling = true;
        Vector3 startPos = transform.position;
        float timer = 0f;

        while (timer < arcTime) {
            float arcDistance = (body.CarryPointPosition - startPos).magnitude * arcFraction;
            Vector3 pos = Vector3.Lerp(startPos, body.CarryPointPosition, timer / arcTime);
            pos.y += Mathf.Sin(Mathf.PI * timer / arcTime) * arcDistance;
            transform.position = pos;
            timer += Time.deltaTime;
            yield return 0;
        }
        transform.position = body.CarryPointPosition;
        travelling = false;
        Invoke("DestroySelf", 0.2f);
    }

    public void ArcToTarget(Vector3 targetPos, bool destroyOnArrival) {
        StartCoroutine(ArcToPosition(targetPos, destroyOnArrival));
    }

    private IEnumerator ArcToPosition(Vector3 targetPos, bool destroyOnArrival) {
        Debug.Log("Arcing...");
        travelling = true;
        Vector3 startPos = transform.position;
        float timer = 0f;
        float arcDistance = (targetPos - startPos).magnitude * arcFraction;

        while (timer < arcTime) {
            Vector3 pos = Vector3.Lerp(startPos, targetPos, timer / arcTime);
            pos.y += Mathf.Sin(Mathf.PI * timer / arcTime) * arcDistance;
            transform.position = pos;
            timer += Time.deltaTime;
            yield return 0;
        }
        transform.position = targetPos;
        travelling = false;
        if (destroyOnArrival) {
            Invoke("DestroySelf", 0.2f);
        }
    }

    protected void DestroySelf() {
        Destroy(gameObject);
    }


}

