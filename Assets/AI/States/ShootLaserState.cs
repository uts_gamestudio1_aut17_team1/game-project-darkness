﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootLaserState : LogicalStateBehaviour {

    private static string LASER_FINISH_TRIGGER = "laserFinished";

    public GameObject laserPrefab;
    public GameObject[] vfxExtras;
    [Space(20)]
    public float laserAttackTime;
    public float laserLength = 5f;
    public float laserTick = 0.5f;
    private float laserTimer;
    private float laserTickTimer;

    private EntityController controller;
    //private GameObject laserObject;
    //private LineRenderer lr;
    private GameObject[] vfxObjects;


    [Space(20)]
    public bool rotateTowardsPlayer = false;
    public float rotateSpeed = 0.2f;

    private bool isInitialised = false;

 
    private void InitialiseState() {
        controller = Animator.gameObject.GetComponent<EntityController>();
        //laserObject = Instantiate(laserPrefab);
        //laserObject.transform.parent = controller.VFXPoint.transform;
        //laserObject.transform.position = controller.VFXPoint.transform.position;
        //lr = laserObject.GetComponent<LineRenderer>();

        vfxObjects = new GameObject[vfxExtras.Length];
        int counter = 0;
        foreach (GameObject go in vfxExtras) {
            Debug.Log("Counter: " + counter);
            vfxObjects[counter] = Instantiate(vfxExtras[counter]);
            vfxObjects[counter].transform.parent = controller.VFXPoint.transform;
            vfxObjects[counter].transform.position = controller.VFXPoint.transform.position;
            counter++;
        }
    }
    protected override void OnStateEntered() {
        if (!isInitialised) {
            InitialiseState();
            isInitialised = true;
        }
        laserTimer = laserAttackTime;
        laserTickTimer = laserTick;
        //laserObject.SetActive(true);
        controller.SoundController.PlayExtraSound(0);

        foreach (GameObject go in vfxObjects) {
            go.SetActive(true);
        }
    }

    private void AttemptLaserHit() {
        Debug.Log("Laser Hit");
        RaycastHit[] hits = Physics.RaycastAll(controller.VFXPoint.transform.position, controller.VFXPoint.transform.forward, laserLength);

        foreach (RaycastHit hit in hits) {
            if (hit.collider.gameObject.CompareTag("Player") || hit.collider.gameObject.CompareTag("PlayerHead")) {
                hit.collider.gameObject.GetComponent<BaseHealth>().TakeDamage(controller.AttackDamage);
            }
        }
    }

    private void UpdateVFXPositions() {
        //Vector3 lrPosition = controller.VFXPoint.transform.forward * laserLength;
        //lrPosition.z = laserLength;
        //lr.SetPosition(1, lrPosition);

        foreach(GameObject go in vfxObjects) {
            go.transform.rotation = controller.VFXPoint.transform.rotation;
        }
    }

    protected override void OnStateUpdated() {
        if (rotateTowardsPlayer) {
            Quaternion targetRotation = Quaternion.LookRotation((controller.TargetPos - controller.Position), Vector3.up);
            controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);

            UpdateVFXPositions();
        }

        laserTickTimer -= Time.deltaTime;
        if (laserTickTimer <= 0) {
            AttemptLaserHit();
            laserTickTimer = laserTick;
        }

        laserTimer -= Time.deltaTime;
        if (laserTimer <= 0) {
            Animator.SetTrigger(LASER_FINISH_TRIGGER);
        }
    }

    protected override void OnStateExited() {
        controller.SoundController.Stop();
        Animator.ResetTrigger(LASER_FINISH_TRIGGER);
        //laserObject.SetActive(false);

        foreach (GameObject go in vfxObjects) {
            go.SetActive(false);
        }
    }

}
