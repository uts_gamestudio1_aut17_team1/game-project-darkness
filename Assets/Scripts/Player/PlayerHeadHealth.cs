﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class PlayerHeadHealth : BaseHealth {

    HUDController hud;

    [SerializeField]
    private int lives = 3;
    private int startingLives;

    private int bodyRessurectCost;

    [SerializeField]
    private SphereCollider lightRadius;
    private Light headLight;
    //private MeshRenderer mr;

    private float maxLightRadius;
    private float maxLightRange;

    private AudioSource source;
    public AudioClip healthSound;

    public Image[] healthIcons;
    [Space(10)]

    public GameObject respawningParticles;

	private PlayerBodyHealth playerBodyHealth;

    public int Lives {
        get { return lives; }
    }

    private void Awake() {
        startingLives = lives;
        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUDController>();
        bodyRessurectCost = maxHealth / 2;
        if (lightRadius == null) {
            lightRadius = GetComponentInChildren<SphereCollider>();
        }
        headLight = GetComponent<Light>();
        // mr = GetComponent<MeshRenderer>();
        source = GetComponent<AudioSource>();

		playerBodyHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBodyHealth> ();
    }

    protected override void Initialise() {
        maxLightRadius = lightRadius.radius;
        maxLightRange = headLight.range;
        base.Initialise();
    }

    public void UpdateHealthUI()
    {
        foreach (Image image in healthIcons)
        {
            if (currentHealth >= int.Parse(image.name.Substring(image.name.Length - 1)))
            {
                image.gameObject.SetActive(true);
            }
            else
            {
                image.gameObject.SetActive(false);
            }
        }
    }

    public override void TakeDamage(int amount) {
        base.TakeDamage(amount);
        UpdateLightIntensity();

        //UpdateHealthUI();
        hud.UpdateHealthUI();
    }

    private void UpdateLightIntensity() {
        float healthPercentage = ((float)currentHealth / (float)maxHealth);
        headLight.range = (healthPercentage * maxLightRange);
        lightRadius.radius = (healthPercentage * maxLightRadius);

        /*
        Color color = mr.material.GetColor("_EmissionColor");
        color.a =  healthPercentage;
        mr.material.SetColor("_EmissionColor", color);
        */
    }

    protected override void Die() {
        GetComponent<PlayerHeadController>().Die();
        lives--;
        hud.UpdateLifeUI();
    }

    public bool CanRessurectBody() {
		if (hasLivesLeft() || currentHealth > bodyRessurectCost) {
			Debug.Log ("Can reserect body");
            return true;
        } else {
			Debug.Log ("Cant reserect body");
            return false;
        }
    }

	public void RessurectBody(){
		TakeDamage(bodyRessurectCost);
		respawningParticles.SetActive(true);
		StartCoroutine(TurnOffParticles(2f));
		Debug.Log ("reserrecting body");
	}

    public override void SetFullHealth() {
        base.SetFullHealth();
        UpdateLightIntensity();

        //UpdateHealthUI();
        hud.UpdateHealthUI();
    }

    public void ResetLives() {
        lives = startingLives;
        hud.UpdateLifeUI();
    }

    public void Heal(int amount) {
        if ((currentHealth += amount) > maxHealth) {
			playerBodyHealth.Heal (currentHealth - maxHealth);
            currentHealth = maxHealth;
        }
        source.PlayOneShot(healthSound, 0.05f);
        UpdateLightIntensity();

        //UpdateHealthUI();
        hud.UpdateHealthUI();
    }

    IEnumerator TurnOffParticles(float time) {
        yield return new WaitForSeconds(time);
        respawningParticles.SetActive(false);
    }

	public bool hasLivesLeft(){
		return lives > 1;
	}

}
