﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyHealth : BaseHealth {

    HUDController hud;

    public AudioClip gruntOne;
    public AudioClip gruntTwo;

    private PlayerBodyController playerBodyController;
    private AudioSource source;
    private int i;

    private void Awake() {
        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUDController>();
        playerBodyController = GetComponent<PlayerBodyController>();
        source = GetComponent<AudioSource>();
        i = 0;
    }

    protected override void Die() {
        playerBodyController.Die();
    }

    public override void TakeDamage(int amount) {
		if (IsDead)
			return;
        if (i == 0)
        {
            source.PlayOneShot(gruntOne, 0.3f);
            i++;
        }
        else
        {
            source.PlayOneShot(gruntTwo, 0.3f);
            i--;
        }

        playerBodyController.DamageAnim();
        hud.FlashDamageOverlay();
		base.TakeDamage (amount);
        hud.UpdateHealthUI();
    }

    public override void SetFullHealth() {
        base.SetFullHealth();
        hud.UpdateHealthUI();
    }

    public void SetHealth(int amount) {
        currentHealth = amount;
        hud.UpdateHealthUI();
    }

	public void Heal(int amount){
		if ((currentHealth += amount) > maxHealth) {
			currentHealth = maxHealth;
		}
		hud.UpdateHealthUI();
	}

}
