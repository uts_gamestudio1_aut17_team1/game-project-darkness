﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class IdleState : LogicalStateBehaviour {
    public bool hasSpecialIdle = false;
    public float specialIdleChance = 2f;

    // Internal counter for checking special idle every 20 frames
    private int specialIdleCounterLimit = 20;
    private int specialIdleCounter = 0;

    [SerializeField]
    private float idleInterval;

    private float timer = 0f;

    protected override void OnStateEntered() {
        timer = idleInterval;
    }

    protected override void OnStateUpdated() {
        if (idleInterval != 0) {
            timer -= Time.deltaTime;
            if (timer <= 0) {
                Animator.SetTrigger("StartAction");
            }
        }
        if (specialIdleCounter == specialIdleCounterLimit)
        {
            if (Random.Range(0f, 100f) <= specialIdleChance)
            {
                Animator.SetTrigger("idleSpecial");
            }
            specialIdleCounter = 0;
        } else
        {
            specialIdleCounter++;
        }
    }

    protected override void OnStateExited()
    {
        Animator.SetTrigger("idleSpecial");
    }
}
