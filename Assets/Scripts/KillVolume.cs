﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillVolume : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.isTrigger)
			return;
		if(other.gameObject.CompareTag("Player")){
			PlayerBodyHealth hp = other.GetComponent<PlayerBodyHealth>();
			hp.TakeDamage(hp.CurrentHealth);

			Debug.Log ("Killed body");
		} else if(other.gameObject.CompareTag("PlayerHead")){
            PlayerHeadHealth hp = other.GetComponent<PlayerHeadHealth>();
            hp.TakeDamage(hp.CurrentHealth);

			Debug.Log ("Killed head");
		}
	}
}
