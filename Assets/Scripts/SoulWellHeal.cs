﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulWellHeal : Arcable {

    public int healAmount = 1;

    public void Heal(PlayerBodyController body) {
        if (!travelling) {
            StartCoroutine(ArcToPlayer(body));
            body.HeadController.HealHead(healAmount);
        }
    }
}
