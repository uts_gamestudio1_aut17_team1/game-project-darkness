﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteScript : MonoBehaviour {

	public GameObject levelCompleteMenu;
	public Text chestsCollectedText;
	public GameObject buttonsHousing;
	public GameObject nextButton;

	public bool hasNextLevel = true;

	private ChestManager chestManager;

	public float noNextLevelMenuPosition = -30;

	public Image whiteOutImage;

	private GameObject playerBody;
	private GameObject playerHead;
	private CameraController cameraController;


	// Use this for initialization
	void Start () {
		chestManager = GameObject.Find ("ChestManager").GetComponent<ChestManager> ();
		playerBody = GameObject.FindGameObjectWithTag ("Player");
		playerHead = GameObject.FindGameObjectWithTag ("PlayerHead");
		cameraController = GameObject.Find ("CameraRig").GetComponent<CameraController> ();

		nextButton.SetActive (hasNextLevel);

		if (!hasNextLevel) {
			buttonsHousing.transform.position = new Vector3 (buttonsHousing.transform.position.x, noNextLevelMenuPosition, buttonsHousing.transform.position.z);
		}
	}

	public void showLevelCompleteMenu(){
		chestsCollectedText.text = chestManager.getChestsCollectedText ();
		levelCompleteMenu.SetActive (true);
		cameraController.enabled = false;
		playerBody.SetActive (false);
		playerHead.SetActive (false);
	}
}
