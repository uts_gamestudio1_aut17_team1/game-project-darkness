﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealth : MonoBehaviour {

    [SerializeField]
    protected int maxHealth = 100;
    [SerializeField]
    protected int currentHealth = 0;

    public int MaxHealth {
        get { return maxHealth; }
    }

    public int CurrentHealth {
        get { return currentHealth; }
    }

    public bool IsDead {
        get { return currentHealth == 0; }
    }

    protected void Start() {
        Initialise();
    }

    protected virtual void Initialise() {
        SetFullHealth();
    }

    public virtual void TakeDamage(int amount) {
		if (!IsDead) {
			if ((currentHealth -= amount) <= 0) {
				currentHealth = 0;
				Die ();
			}
			//Debug.Log(gameObject.name + " hit");
		}
    }

    protected virtual void Die() {
  		      Destroy(gameObject);
    }

    public virtual void SetFullHealth() {
        currentHealth = maxHealth;
    }
    
}
