﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPoints : MonoBehaviour { 

    HUDController hud;

    private int gold = 0;

    private void Awake() {
        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUDController>();
    }

    public void updatePoints(int value)
    {
        gold += value;
        hud.UpdateCoinValue(value);
    }

    public int getGold()
    {
        return gold;
    }
}
