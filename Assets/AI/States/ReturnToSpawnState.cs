﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class ReturnToSpawnState : LogicalStateBehaviour {
    private EntityController entityController;

    [SerializeField]
    private float DistanceToReturnMargin = 0.1f;

    protected override void OnStateEntered() {
        entityController = Animator.gameObject.GetComponent<EntityController>();
        entityController.NavmeshAgent.isStopped = false;
        entityController.NavmeshAgent.SetDestination(entityController.SpawnPos);
        entityController.NavmeshAgent.stoppingDistance = 0f;
    }

    protected override void OnStateUpdated() {

        /*
        entityController.ReturnToSpawn();
        if (Vector3.Distance(entityController.transform.position, entityController.TargetPos) < DistanceToReturnMargin) {
            Animator.SetTrigger("returned");
        }
        */
        //entity.position = Vector3.MoveTowards(entity.position, entityController.SpawnPos, Time.deltaTime * entityController.MovementSpeed);

        if (entityController.NavmeshAgent.pathStatus == NavMeshPathStatus.PathComplete) {
            Animator.SetTrigger("returned");
        }
    }

    protected override void OnStateExited() {
        entityController.NavmeshAgent.stoppingDistance = entityController.DistanceToTargetMargin;
        //entityController.CancelMovement();
    }
}
