﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableController : MonoBehaviour {
	[Header("Drop Settings")]
	public int numberOfDrops = 5;
	public GameObject healthDrop;

	[Header("Death Settings")]
	public float explosionDelay = 0.1f;
	public GameObject explosionPrefab;

    public AudioClip smashSound;
    private AudioSource source;

	void Awake() {
        source = GetComponent<AudioSource>();
	}

	public virtual IEnumerator Die() {
		if (explosionPrefab) {
			Instantiate(explosionPrefab, transform.position, explosionPrefab.transform.rotation);
		}
        source.PlayOneShot(smashSound);
		yield return new WaitForSeconds(explosionDelay);
		if (healthDrop) {
			for (int i = 0; i < numberOfDrops; i++) {
				GameObject health = Instantiate(healthDrop, transform.position, healthDrop.transform.rotation);
				health.GetComponent<PickupLife>().ArcToTarget(transform.position + new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)), false);
			}          
		}
		Destroy(gameObject);
	}

}