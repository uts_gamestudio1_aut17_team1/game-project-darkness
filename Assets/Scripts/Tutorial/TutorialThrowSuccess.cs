﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialThrowSuccess : MonoBehaviour {

    public TutorialController tc;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("PlayerHead")) {
            tc.ObjectThrown = true;
        }
    }
}
