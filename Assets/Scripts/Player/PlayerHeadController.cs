﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class PlayerHeadController : MonoBehaviour {

    private bool isCarried = false;

    private PlayerHeadHealth health;
    private CheckpointManager checkpointManager;
	private DeathMenuController deathMenuController;
	private Rigidbody playerRgidbody;


    private int deathCounter = 0;

	private SceneFading sceneFader;

    private void Awake() {
        health = GetComponent<PlayerHeadHealth>();
        checkpointManager = GameObject.Find("CheckpointManager").GetComponent<CheckpointManager>();
		sceneFader = GameObject.Find ("FadeManager").GetComponent<SceneFading> ();
		deathMenuController = GameObject.Find ("HUD").GetComponent<DeathMenuController> ();
		playerRgidbody = GetComponent<Rigidbody> ();
    }

    public bool IsCarried {
        get { return isCarried; }
        set { isCarried = value; }
    }

    public int DeathCount {
        get { return deathCounter; }
    }

    public bool RessurectBody() { 
		bool canRessurectPlayer = health.CanRessurectBody();
		if (canRessurectPlayer) {
			health.RessurectBody ();
		} else {
			Debug.Log ("Player out of lives and dead");
			health.TakeDamage (health.CurrentHealth * 2);
		}
		return canRessurectPlayer;
    }

	private void activateDeathMenu(){
		deathMenuController.activateDeathMenu ();
	}

//	private void resetPlayerLivesAndHealth(){
//		health.ResetLives ();
//		health.SetFullHealth ();
//		checkPointRespawn ();
//	}

    public void Die() {
        deathCounter++;
        Analytics.CustomEvent("Player Head Died", new Dictionary<string, object> {
            {"Level", SceneManager.GetActiveScene().name},
            {"Position", transform.position }
        });
		if (health.hasLivesLeft ()) {
			sceneFader.spotFade (4f);
			Invoke ("checkPointRespawn", 2f);
		} else {
			sceneFader.spotFade (8f);
			Invoke ("activateDeathMenu", 4);
		}
		playerRgidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

	private void checkPointRespawn(){
		checkpointManager.RespawnPlayer();
	}

    public void Respawn() {
        GetComponent<Collider>().enabled = false;
        health.SetFullHealth();
        GetComponent<Collider>().enabled = true;
		playerRgidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
    }

    public void HealHead(int amount) {
        health.Heal(amount);
    }

}
