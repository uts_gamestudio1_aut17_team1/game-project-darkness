﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EntitySoundController : MonoBehaviour {

    public float volume = 0.05f;

    public AudioClip aggroSound;
    public AudioClip[] attackSounds;
    public AudioClip[] hurtSounds;
    public AudioClip deathSound;
    public AudioClip[] extraSounds;

    private AudioSource source;

    private void Awake() {
        source = GetComponent<AudioSource>();
    }

    public void PlayAggroSound() {
        if (aggroSound) {
            source.PlayOneShot(aggroSound, volume);
        }
    }

    public void PlayAttackSound() {
        if (attackSounds.Length != 0) {
            int rand = Random.Range(0, attackSounds.Length - 1);
            source.PlayOneShot(attackSounds[rand], volume);
        }
    }

    public void PlayHurtSound() {
        if (hurtSounds.Length != 0) {
            int rand = Random.Range(0, hurtSounds.Length - 1);
            source.PlayOneShot(hurtSounds[rand], volume);
        }
    }

    public void PlayDeathSound() {
        if (deathSound) {
            source.PlayOneShot(deathSound, volume);
        }
    }

    public void PlayExtraSound(int i) {
        if (i <= extraSounds.Length - 1) {
            source.PlayOneShot(extraSounds[i], volume);
        }
    }

    public void Stop() {
        source.Stop();
    }

}
