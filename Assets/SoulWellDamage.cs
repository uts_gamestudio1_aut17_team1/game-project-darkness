﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulWellDamage : Arcable {

    public int damageAmount = 1;

    public void Damage(PlayerBodyController body, SoulWell well) {
        if (!travelling) {
            ArcToTarget(well.gameObject.transform.position, true);
            body.HeadController.GetComponent<PlayerHeadHealth>().TakeDamage(damageAmount);
        }
    }
}
