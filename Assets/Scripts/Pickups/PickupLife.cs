﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class PickupLife : Arcable {

    [Header("Heal Settings")]
    public int healthValue = 10;
    [Header("Pickup Range")]
    public float pickupRange = 5f;

    private void Awake() {
        GetComponent<SphereCollider>().radius = pickupRange;
    }

    private void OnTriggerStay(Collider other) {
        if (!travelling) {
            if (other.CompareTag("Player")) {
                PlayerBodyController body = other.gameObject.GetComponent<PlayerBodyController>();
                if (body.CarryingHead) {
                    StartCoroutine(ArcToPlayer(body));
                    body.HeadController.HealHead(healthValue);
                }
            }
        }
    }
}
