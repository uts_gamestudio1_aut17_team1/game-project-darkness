﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathMenuController : MonoBehaviour {

	public GameObject deathMenu;

	private GameObject playerBody;

	void Awake(){
		playerBody = GameObject.FindGameObjectWithTag ("Player");
	}

	public void activateDeathMenu(){
		deathMenu.SetActive (true);
		playerBody.GetComponent<PlayerMovement> ().enabled = false;
	}

	public void unpauseOnSelection(){
		Time.timeScale = 1;
	}
}
