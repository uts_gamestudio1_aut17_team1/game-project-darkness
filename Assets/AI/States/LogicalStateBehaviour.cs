﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicalStateBehaviour : StateMachineBehaviour {

    // Internal overrides
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Animator = animator;
        OnStateEntered();
        active = true;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Animator = animator;
        active = false;
        OnStateExited();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        OnStateUpdated();
    }

    // State behavior additions
    private bool active = false;

    protected Animator Animator {
        get;
        private set;
    }

    void OnDisable() {
        if (active) {
            OnStateExited();
        }
    }

    protected virtual void OnStateEntered() { }
    protected virtual void OnStateUpdated() { }
    protected virtual void OnStateExited() { }
}
