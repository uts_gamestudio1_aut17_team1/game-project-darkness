﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject cursor;

    Animator anim;

    public float speed = 5f;
	public float maxDistanceFromHead = 10f;

	private GameObject playerHead;

    Vector3 movement;
    Rigidbody playerRigidbody;
    int floorMask; // Layer raycast can collide with
    float camRayLength = 100f; // max distance raycast checks for collisions
	Vector3 mouseTargetPoint;

	int groundMask;

	bool canJump;
	bool hasJumped;
	float jumpForce = 6;

    bool inputEnabled = true;

	public bool usingController = false;

    public bool InputEnabled {
        get { return inputEnabled; }
        set { inputEnabled = value; }
    }

    public bool CanJump {
        set { canJump = value; }
    }

    public bool HasJumped {
        get { return hasJumped; }
    }

    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
		playerHead = GameObject.FindGameObjectWithTag ("PlayerHead");
		groundMask = LayerMask.GetMask ("LevelStructure");

        anim = transform.Find("Player_Character").GetComponent<Animator>();
        anim.SetBool("Walking", false);

		if (!usingController) {
			cursor.SetActive (false);
		}
    }

    void FixedUpdate()
    {
        if (inputEnabled) {
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");

            Move(h, v);
            Turn();
        }
    }
    
    void Move(float h, float v)
    {
		if (!anim.GetBool("Walking") && (h != 0 || v != 0))
        {
            anim.SetBool("Walking", true);
        } else if (anim.GetBool("Walking") && h == 0 && v == 0)
        {
            anim.SetBool("Walking", false);
        }
        
        //playerRigidbody.velocity = Vector3.zero;

        movement.Set(h, transform.position.y, v);
        movement = movement.normalized * speed * Time.deltaTime;
		Vector3 newPosition = transform.position + movement;

		//Clamp the players position so it is in range of the head
		newPosition.x = Mathf.Clamp (newPosition.x, playerHead.transform.position.x - maxDistanceFromHead, playerHead.transform.position.x + maxDistanceFromHead);
		newPosition.z = Mathf.Clamp (newPosition.z, playerHead.transform.position.z - maxDistanceFromHead, playerHead.transform.position.z + maxDistanceFromHead);

		float testDistance = 1.1f;
		Ray groundCheckRay = new Ray (transform.position + Vector3.up, Vector3.down);
		RaycastHit hit;
		Debug.DrawRay (transform.position+ Vector3.up, Vector3.down * testDistance);
		if (!Physics.Raycast (groundCheckRay, out hit, testDistance, groundMask)) {
			newPosition += Vector3.down * speed * Time.deltaTime;
		} else {
			canJump = true;
			hasJumped = false;
		}

		if ((Input.GetButtonDown("B") || Input.GetKeyDown (KeyCode.Space)) && canJump && !hasJumped) {
			playerRigidbody.velocity = Vector3.up * jumpForce;
			canJump = false;
			hasJumped = true;
		}


        playerRigidbody.MovePosition(newPosition);
    }

    // rotate player to face the mouse
    void Turn()
    {
        // cast ray from the camera to the mouse's position on the screen
		Ray camRay;
		if(usingController){
			camRay = Camera.main.ScreenPointToRay(cursor.GetComponent<CustomCursor>().getCursorPosition()); //Input.mousePosition
		} else {
			camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		}
        RaycastHit floorHit;

        // if ray collided with something
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
			//Store for use in throughing the head.
			mouseTargetPoint = floorHit.point;
            // vector from player's position to point hit by ray 
            Vector3 playerToMouse = floorHit.point - transform.position;

            // remove y as we do not need it
            playerToMouse.y = 0f;

            // rotate player toward the mouse
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

	public Vector3 MouseTargetPoint(){
		return mouseTargetPoint;
	}
}