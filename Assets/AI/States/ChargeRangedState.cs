﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class ChargeRangedState : LogicalStateBehaviour {

    private static string RANGE_CHARGED_TRIGGER = "rangeCharged";
    private EntityController controller;

    public GameObject chargePrefab;
    public float chargeTime;

    private float chargeTimer;
    private GameObject chargeObject;

    [Space(20)]
    public bool rotateTowardsPlayer = false;
    public float rotateSpeed = 0.1f;

    private bool isInitialised = false;

    private void InitialiseState() {
        controller = Animator.gameObject.GetComponent<EntityController>();
        controller.NavmeshAgent.isStopped = true;
        chargeObject = Instantiate(chargePrefab);
        chargeObject.transform.position = controller.VFXPoint.transform.position;
        chargeObject.transform.parent = controller.VFXPoint.transform;
    }

    protected override void OnStateEntered() {
        if (!isInitialised) {
            InitialiseState();
            isInitialised = true;
        }

        chargeTimer = chargeTime;
        chargeObject.SetActive(true);
    }

    protected override void OnStateUpdated() {
        if (rotateTowardsPlayer) {
            Quaternion targetRotation = Quaternion.LookRotation((controller.TargetPos - controller.Position), Vector3.up);
            controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
        }


        chargeTimer -= Time.deltaTime;

        if (chargeTimer <= 0) {
            Animator.SetTrigger(RANGE_CHARGED_TRIGGER);
            chargeTimer = chargeTime;
        }
    }

    protected override void OnStateExited() {
        chargeObject.SetActive(false);
    }
}
