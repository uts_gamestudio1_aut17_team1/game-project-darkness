﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EntityController : MonoBehaviour {

    private NavMeshAgent navmeshAgent;
    [Header("Optional VFX Attachment")]
    [SerializeField]
    private GameObject vfxPoint;
    [Space(20)]

    [Header("Movement Settings")]
    [SerializeField]
    private float defaultMovementSpeed = 5f;
    [Space(10)]
    private float currentMovementSpeed;

    [SerializeField]
    private float distanceToTargetMargin = 2f;


    private Vector3 spawnPoint;
    [Header("Debug")]
    public Transform target = null;
    [SerializeField]
    private List<Transform> possibleTargets = new List<Transform>();
    private List<Transform> removeTargets = new List<Transform>();
    private Vector3 targetPos;

    //private Rigidbody rb;
    private Animator stateController;

    [Header("Attack Settings")]
    [SerializeField]
    private int attackDamage = 10;
    [SerializeField]
    private float attackInterval = 1.5f;

    [Header("Drop Settings")]
    public int numberOfDrops = 1;
    public GameObject healthDrop;

    private EntitySoundController soundController;

    public GameObject VFXPoint {
        get { return vfxPoint; }
    }

    public Transform Target {
        get { return target; }
        set { target = value; }
    }

    public Vector3 SpawnPos {
        get { return spawnPoint; }
    }

    public Vector3 TargetPos {
        get { return targetPos; }
    }

    public float MovementSpeed {
        get { return currentMovementSpeed; }
    }

    public float DistanceToTargetMargin {
        get { return distanceToTargetMargin; }
    }

    public int AttackDamage {
        get { return attackDamage; }
    }

    public float AttackInterval {
        get { return attackInterval; }
    }

    public Vector3 Position {
        get { return transform.position; }
    }

    public NavMeshAgent NavmeshAgent {
        get { return navmeshAgent; }
    }

    public EntitySoundController SoundController {
        get { return soundController; }
    }

    void Awake() {
        // rb = GetComponent<Rigidbody>();
        stateController = GetComponent<Animator>();
        navmeshAgent = GetComponent<NavMeshAgent>();
        soundController = GetComponent<EntitySoundController>();
    }

    void Start() {
        currentMovementSpeed = defaultMovementSpeed;

        navmeshAgent.stoppingDistance = distanceToTargetMargin;
        navmeshAgent.speed = currentMovementSpeed;
        spawnPoint = transform.position;
    }


    void FixedUpdate() {
        if (possibleTargets.Count >= 1) {
            PickTarget();
            UpdateTargetPosition();
            CheckAttackRange();
        }


        /*

        if (target != null) {
            if (Vector3.Distance(targetPos, transform.position) > distanceToTargetMargin) {
                transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * currentMovementSpeed);
                transform.LookAt(target);
                stateController.SetBool("targetInAttackRange", false);
            } else {
                stateController.SetBool("targetInAttackRange", true);
            }            
        }

        if (targetPos == spawnPoint) {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * currentMovementSpeed);
            transform.LookAt(spawnPoint);
        }
        */

    }

    protected void CheckAttackRange() {
        if (Target) {
            if (Vector3.Distance(Target.position, transform.position) <= distanceToTargetMargin && IsTargetInfront()) {
                stateController.SetBool("targetInAttackRange", true);
            } else {
                stateController.SetBool("targetInAttackRange", false);
            }
        }
    }

    public bool IsTargetInfront() {
        /*
        Vector3 relativePoint = transform.InverseTransformPoint(Target.position);
        Debug.Log("Relative Z: " + relativePoint.z);
        return relativePoint.z >= 0.2 && relativePoint.z <= 0.4;
        */
        Vector3 directionToTarget = transform.position - Target.position;
        float angle = Vector3.Angle(transform.forward, directionToTarget);

        return Mathf.Abs(angle) > 90;
    }

    protected void UpdateTargetPosition() {
        if (target != null) {
            targetPos = target.position;
        }
    }

    public void DamageTarget() {
        if (target != null) {
            BaseHealth health = target.gameObject.GetComponent<BaseHealth>();
            health.TakeDamage(attackDamage);

            if (health.IsDead) {
                removeTargets.Add(health.gameObject.transform);
                target = null;
                stateController.SetBool("targetInAttackRange", false);
            }
        }
    }

    protected void PickTarget() {
        RemoveOldTargets();

        Transform closestTarget = null;
        foreach (Transform t in possibleTargets) {
            if (t.CompareTag("PlayerHead")) {
                if (t.GetComponent<PlayerHeadController>().IsCarried == false) {
                    closestTarget = t;
                    break;
                }
            } else {
                if (closestTarget != null) {
                    if (Vector3.Distance(transform.position, t.position) < Vector3.Distance(transform.position, closestTarget.position)) {
                        closestTarget = t;
                    }
                } else {
                    closestTarget = t;
                }
            }
        }

        target = closestTarget;
        if (target != null) {
            stateController.SetBool("targetInRange", true);
        } else {
            stateController.SetBool("targetInRange", false);
        }
    }

    protected void RemoveOldTargets() {
        foreach (Transform t in removeTargets) {
            possibleTargets.Remove(t);
        }
        removeTargets.Clear();
    }

    public void AddAggroTarget(Collider other) {
        if (!possibleTargets.Contains(other.gameObject.transform)) {
            if (!other.CompareTag("Weapon") && !other.CompareTag("Pickupable")) {
                possibleTargets.Add(other.gameObject.transform);
                PickTarget();
            }
        }
    }

    public void RemoveAggroTarget(Collider other) {
        if (possibleTargets.Contains(other.gameObject.transform)) {
            removeTargets.Add(other.gameObject.transform);
            PickTarget();
        }
    }

    public virtual IEnumerator Die() {
        soundController.PlayDeathSound();
        stateController.SetTrigger("die");
        yield return new WaitForSeconds(3f);
        if (healthDrop) {
            for (int i = 0; i < numberOfDrops; i++) {
                GameObject health = Instantiate(healthDrop, Position, healthDrop.transform.rotation);
                health.GetComponent<PickupLife>().ArcToTarget(Position + new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)), false);
            }          
        }
        Destroy(gameObject);
    }

}
