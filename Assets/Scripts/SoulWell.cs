﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulWell : MonoBehaviour {
    [Header("Prefabs")]
    public GameObject HealSoul;
    public GameObject DamageSoul;
    [Header("Children")]
    public GameObject healingSouls;
    public GameObject damagingSouls;
    [Header("Options")]
    public float soulRate = 0.5f;
    private float timer = 0f;
    public bool isTrap = false;

    private void Awake() {
        if (isTrap) {
            damagingSouls.SetActive(true);
        } else {
            healingSouls.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            PlayerBodyController body = other.GetComponent<PlayerBodyController>();
            if (body.CarryingHead) {
                timer -= Time.deltaTime;
                if (timer <= 0) {
                    if (!isTrap) {
                        GameObject soul = Instantiate(HealSoul, healingSouls.transform.position, HealSoul.transform.rotation);
                        soul.GetComponent<SoulWellHeal>().Heal(body);                        
                    } else {
                        GameObject soul = Instantiate(DamageSoul, body.HeadController.gameObject.transform.position, DamageSoul.transform.rotation);
                        soul.GetComponent<SoulWellDamage>().Damage(body, this);
                    }
                    timer = soulRate;
                }

            }
        }
    }
}
