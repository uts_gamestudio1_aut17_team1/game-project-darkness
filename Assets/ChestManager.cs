﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestManager : MonoBehaviour {

	private int numChestsInLevel;
	private int numChestsFound;

	private HUDController hudController;

	void Start(){
		hudController = GameObject.Find ("HUD").GetComponent<HUDController> ();
	}

	public void registerChestToBeFound(){
		numChestsInLevel++;
		hudController.UpdateCoinValue (0);
	}

	public void chestFound(){
		numChestsFound++;
		hudController.UpdateCoinValue (0);
	}

	public int getNumChestsInLevel(){
		return numChestsInLevel;
	}

	public int getNumChestsFound(){
		return numChestsFound;
	}

	public string getChestsCollectedText(){
		return numChestsFound + " of " + numChestsInLevel + " chests found";
	}
}
