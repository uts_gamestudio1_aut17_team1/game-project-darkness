﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableHealth : BaseHealth{
	private DestructableController controller;

	void Awake()
	{
		controller = GetComponent<DestructableController> ();

	}

	public override void TakeDamage(int amount)
	{
		base.TakeDamage(amount);
		//controller.SoundController.PlayHurtSound();
	}

	protected override void Die() {
		StartCoroutine(controller.Die());        
	}
}


