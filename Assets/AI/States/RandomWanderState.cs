﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class RandomWanderState : LogicalStateBehaviour {
    [SerializeField]
    private float maxWanderDistance;
    [SerializeField]
    private float distanceToTargetMargin = 0.005f;

    private Transform transform;
    private Vector3 targetPos;

    protected override void OnStateEntered() {
        transform = Animator.gameObject.transform;
        targetPos = new Vector3(transform.position.x + Random.Range(-maxWanderDistance, maxWanderDistance), transform.position.y, transform.position.z + Random.Range(-maxWanderDistance, maxWanderDistance));
    }
    protected override void OnStateUpdated() {
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime);

        if (Vector3.Distance(transform.position, targetPos) < distanceToTargetMargin) {
            Animator.SetTrigger("EndWander");
        }
    }
}
