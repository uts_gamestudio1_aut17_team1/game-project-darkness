﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDamage : MonoBehaviour
{
	public bool active;
	public int trapDamage;


	void Start ()
	{
	}

	public void setActive (bool state)
	{
		active = state;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.isTrigger)
			return;
		BaseHealth baseHealth = other.transform.gameObject.GetComponent<BaseHealth> ();
		if (baseHealth != null) {
			baseHealth.TakeDamage (trapDamage);
		}
	}
}
