﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour {

    private const float NextMessageInterval = 2f;

    private const string LOSE_SOUL = "Try not to lose your soul...";
    private const string LOSE_BODY = "Try to stay on the platform...";

    private const string STAGE_ONE_TEXT_1 = "Welcome! I'm sure you're quite confused as to how you've ended up in this place. Unfortunately, I'm not here to answer that question. However, I am here to get you on your feet.";
    private const string STAGE_ONE_TEXT_2 = "To move around, use the W-A-S-D keys or the left joystick. Give it a go!";
    private const string STAGE_ONE_TEXT_3 = "Good job!";
    private const string STAGE_ONE_TEXT_4 = "Now, try looking around by moving the mouse around or using the right joystick.";
    private const string STAGE_ONE_TEXT_5 = "Great!";
    private const string STAGE_ONE_TEXT_6 = "Finally, you can press [SPACE] or (B) to jump!";

    private const string STAGE_TWO_TEXT_1 = "Now, you're probably wondering what that glowing crystal is. That is your soul.\nWhen you lost your head, your soul seeped out of your body and solidified into a soul crystal. You must protect this, as it is your LIFE and your only SOURCE OF LIGHT!";
    private const string STAGE_TWO_TEXT_2 = "To pick your soul up, walk over to it and click the MIDDLE MOUSE BUTTON (SCROLL WHEEL) or (X).";
    private const string STAGE_TWO_TEXT_3 = "To put it down again, press the same button.";
    private const string STAGE_TWO_TEXT_4 = "Nice work! Be sure to keep in mind that leaving it on the ground makes it vulnerable to attack. Who knows what lurks in the dark...\nAnd another thing - you can't jump when carrying your soul... but there is a solution!";
    private const string STAGE_TWO_TEXT_5 = "You can also throw your soul crystal. Hold down RIGHT MOUSE BUTTON or (Y) to aim, and then simply let go to throw it! \nTry throwing it onto the red platform.";
    private const string STAGE_TWO_TEXT_6 = "Great throw!";
    private const string STAGE_TWO_TEXT_7 = "You'll probably need that back...";

    private const string STAGE_THREE_TEXT_1 = "You've no doubt noticed that you have a sword, but it's only unsheathed when not carrying your soul.\nYou can only attack when your hands are not otherwise occupied. It's up to you to decide what to in different situations.\nTo attack press the LEFT MOUSE BUTTON or (A)";
    private const string STAGE_THREE_TEXT_2 = "Let's try swinging that sword!";
    private const string STAGE_THREE_TEXT_3 = "Behold my power as I summon you an enemy...boxes!\nDestroy them!";
    private const string STAGE_THREE_TEXT_4 = "Great work dispatching those boxes. However, they didn't pose a threat, and there are things out there that do. Let's learn a little about health...";
    private const string STAGE_THREE_TEXT_5 = "If your soul takes damage, it will reduce the effectiveness of its light source.";
    private const string STAGE_THREE_TEXT_6 = "As you can see, the light source is slightly diminished now.";
    private const string STAGE_THREE_TEXT_7 = "Let's restore that to its previous state...";
    private const string STAGE_THREE_TEXT_8 = "If your body dies it can be ressurected by your soul, provided there is enough power left inside it. Let's give it a try!";
    private const string STAGE_THREE_TEXT_9 = "That looks like it hurt! As you can see, your soul ressurected your body, but at a cost. But there is a way to replenish your soul...";
    private const string STAGE_THREE_TEXT_10 = "Here's some more boxes. These boxes drop soul fragments which heal your soul. Enemies also drop these.\nTo collect these fragments, you must hold your soul.";
    private const string STAGE_THREE_TEXT_11 = "All better!";

    private const string STAGE_FOUR_TEXT_1 = "The prospect of dying in your adventures does seem scary. There is a way to save progress though!";
    private const string STAGE_FOUR_TEXT_2 = "In your travels you'll come across checkpoints that your soul can light up.\nIf you die, you'll respawn at one.";
    private const string STAGE_FOUR_TEXT_3 = "Behold - Fire!";
    private const string STAGE_FOUR_TEXT_4 = "That's all you need to know out there. Go collect treasures and fight your way through enemies and boxes. Just try not to lose your head!\nOff to a great start aren't we...";
    private const string STAGE_FOUR_TEXT_5 = "Oh, of course. To progress, you must find your way to a special way-gate. I'll help you along on this one.";
    private const string STAGE_FOUR_TEXT_6 = "Good luck!";


    private enum TutorialStage {
        None,
        One,
        Two,
        Three,
        Four
    }

    [SerializeField]
    public KeyCode continueButton = KeyCode.Space;

    [Header("Tutorial Stage")]
    [SerializeField]
    private TutorialStage stage = TutorialStage.None;

    [Header("Main Objects")]
    public GameObject player;
    public GameObject soulCrystal;
    private PlayerMovement playerMovement;
    private PlayerBodyController pbc;
    private PlayerBodyHealth bodyHealth;
    private PlayerHeadController phc;
    private PlayerHeadHealth headHealth;

    private CustomCursor customCursor;
    private MessagePanelController mpc;

    private bool expectingContinue = false;

    // Stage 1 Tutorial
    private bool forwardPressed, backPressed, leftPressed, rightPressed = false;
    private bool movementComplete = false;
    private Vector3 customCursorStartPos;
    private bool mouseXAxisMove, mouseYAxisMove = false;
    private float moveThreshhold = 0.1f;
    private bool mouseMovementComplete = false;
    private float mouseMovementCompletePause = 2f;
    private bool hasJumped = false;

    // Stage 2 Tutorial
    private bool soulPickedUp = false;
    private bool soulDropped = false;

    [Header("Stage Two Objects")]
    public GameObject throwPlatform;

    private bool objectThrown = false;
    private bool throwComplete = false;

    public bool ObjectThrown {
        set { objectThrown = value; }
    }

    // Stage 3 Tutorial
    [Header("Stage Three Objects")]
    public GameObject boxSpawner;
    public GameObject boxPrefab;
    public GameObject boxHealPrefab;
    public int numberofBoxes = 3;
    public int numberofHealingBoxes = 2;
    public GameObject[] boxes;
    private bool boxesSummoned = false;
    private bool boxesDestroyed = false;
    private bool killedPlayer = true;
    private bool playerRessurected = false;
    private bool playerHealed = false;

    // Stage 4 Tutorial
    [Header("Stage Four Objects")]
    public GameObject checkpointPlatform;
    public CheckpointController checkpoint;
    public GameObject cheatBattier;
    public GameObject gateWall;
    public GameObject gateParticles;
    public BoxCollider gateCollider;
    private bool moveGate = false;
    Vector3 gateWallTargetPos;


    private void Awake() {
        playerMovement = player.GetComponent<PlayerMovement>();
        playerMovement.InputEnabled = false;
        playerMovement.CanJump = false;

        mpc = GameObject.Find("MessageScreen").GetComponent<MessagePanelController>();
        mpc.ContinueButton = continueButton;

        customCursor = GameObject.Find("Cursor").GetComponent<CustomCursor>();

        pbc = player.GetComponent<PlayerBodyController>();
        pbc.CanAttack = false;
        pbc.CanCarry = false;
        phc = soulCrystal.GetComponent<PlayerHeadController>();
        headHealth = soulCrystal.GetComponent<PlayerHeadHealth>();
        bodyHealth = player.GetComponent<PlayerBodyHealth>();

        boxes = new GameObject[numberofBoxes];

        gateWallTargetPos = gateWall.transform.position - new Vector3(0, 3, 0);
        SetEndObjectsActive(false);
    }

    private void Start() {
        StartCoroutine(StageOneTutorial());
    }

    private void KeyForContinue() {
        playerMovement.CanJump = false;
        expectingContinue = true;
        mpc.ContinueTextActive(true);
    }

    private void Update() {

        if (expectingContinue) {
            if (Input.GetKeyDown(continueButton)) {
                expectingContinue = false;
                mpc.ContinueTextActive(false);
                if (stage != TutorialStage.One) {
                    playerMovement.CanJump = true;
                }
            }
        }

        if (stage == TutorialStage.One) {
            StageOneChecks();
        }

        if (stage == TutorialStage.Two) {
            StageTwoChecks();
        }

        if (stage == TutorialStage.Three) {
            StageThreeChecks();
        }

        if (stage == TutorialStage.Four) {
            StageFourChecks();
        }
    }

    public void SoulFallen() {
        mpc.FlashText(LOSE_SOUL, 2f);
        pbc.PickupObject(soulCrystal);
    }

    public void BodyFallen() {
        mpc.FlashText(LOSE_BODY, 2f);
        pbc.ResetToSpawn();
    }

#region Stage 1
    private void StageOneChecks() {
        if (!movementComplete) {
            CheckStageOneInputs();
        } else if (!mouseMovementComplete) {
            CheckStageOneMouseInputs();
        } else if (!hasJumped) {
            if (playerMovement.HasJumped) {
                hasJumped = true;
            }
        }
    }
    private void CheckStageOneInputs() {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetAxis("LeftJoystickY") > moveThreshhold) {
            forwardPressed = true;
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetAxisRaw("LeftJoystickY") < moveThreshhold) {
            backPressed = true;
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetAxisRaw("LeftJoystickX") < moveThreshhold) {
            leftPressed = true;
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetAxisRaw("LeftJoystickX") > moveThreshhold) {
            rightPressed = true;
        }

    }

    private void CheckStageOneMouseInputs() {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float horizontal = Input.GetAxisRaw("RightJoystickX");
        float vertical = Input.GetAxis("RightJoystickY");

        if (mouseX > moveThreshhold || horizontal > moveThreshhold) {
            mouseXAxisMove = true;
        }

        if (mouseY > moveThreshhold || vertical > moveThreshhold) {
            mouseYAxisMove = true;
        }
    }

    private IEnumerator StageOneTutorial() {
        stage = TutorialStage.One;
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.ShowPanel();
        mpc.SetText(STAGE_ONE_TEXT_1);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_ONE_TEXT_2);
        playerMovement.InputEnabled = true;
        yield return new WaitUntil(() => (forwardPressed == true && backPressed == true && leftPressed == true && rightPressed == true));
        movementComplete = true;
        mpc.SetText(STAGE_ONE_TEXT_3);
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_ONE_TEXT_4);
        yield return new WaitUntil(() => mouseXAxisMove == true && mouseYAxisMove == true);
        yield return new WaitForSeconds(mouseMovementCompletePause);
        mouseMovementComplete = true;
        mpc.SetText(STAGE_ONE_TEXT_5);
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_ONE_TEXT_6);
        playerMovement.CanJump = true;
        yield return new WaitUntil(() => hasJumped == true);
        StartCoroutine(StageTwoTutorial());
    }
    #endregion

    #region Stage 2
    private void StageTwoChecks() {
        if (!soulPickedUp) {
            if (pbc.CarryingHead) {
                soulPickedUp = true;
            }
        } else if (!soulDropped) {
            if (!pbc.CarryingHead) {
                soulDropped = true;
            }
        } else if (!throwComplete) {
            if (objectThrown) {
                throwComplete = true;
            }
        }
    }


    private IEnumerator StageTwoTutorial() {
        stage = TutorialStage.Two;
        mpc.SetText(STAGE_TWO_TEXT_1);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_TWO_TEXT_2);
        pbc.CanCarry = true;
        yield return new WaitUntil(() => soulPickedUp == true);
        mpc.SetText(STAGE_TWO_TEXT_3);
        yield return new WaitUntil(() => soulDropped == true);
        mpc.SetText(STAGE_TWO_TEXT_4);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_TWO_TEXT_5);
        throwPlatform.SetActive(true);
        yield return new WaitWhile(() => throwComplete == false);
        mpc.SetText(STAGE_TWO_TEXT_6);
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_TWO_TEXT_7);
        pbc.PickupObject(soulCrystal);
        throwPlatform.SetActive(false);
        StartCoroutine(StageThreeTutorial());
    }

    #endregion

#region Stage 3
    private void StageThreeChecks() {
        if (boxesSummoned && !boxesDestroyed) {
            int count = 0;
            for (int i = 0; i < numberofBoxes; i++) {
                if (boxes[i] == null) {
                    count++;
                }
            }
            if (count == numberofBoxes) {
                boxesDestroyed = true;
            }
        } else if (killedPlayer && !playerRessurected) {
            if (!bodyHealth.IsDead)
                playerRessurected = true;
        } else if (playerRessurected && !playerHealed) {
            if (headHealth.CurrentHealth == headHealth.MaxHealth) {
                playerHealed = true;
            }
        }
    }

    private IEnumerator StageThreeTutorial() {
        stage = TutorialStage.Three;
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_THREE_TEXT_1);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_THREE_TEXT_2);
        yield return new WaitForSeconds(NextMessageInterval);
        pbc.CanAttack = true;
        mpc.SetText(STAGE_THREE_TEXT_3);
        StartCoroutine(SummonBoxes());
        yield return new WaitUntil(() => boxesDestroyed == true);
        mpc.SetText(STAGE_THREE_TEXT_4);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_THREE_TEXT_5);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        headHealth.TakeDamage(1);
        mpc.SetText(STAGE_THREE_TEXT_6);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_THREE_TEXT_7);
        headHealth.SetFullHealth();
        playerHealed = true;
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_THREE_TEXT_8);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        bodyHealth.TakeDamage(bodyHealth.MaxHealth);
        yield return new WaitUntil(() => playerRessurected == true);
        mpc.SetText(STAGE_THREE_TEXT_9);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_THREE_TEXT_10);
        StartCoroutine(SummonHealingBoxes());
        playerHealed = false;
        yield return new WaitUntil(() => playerHealed == true);
        mpc.SetText(STAGE_THREE_TEXT_11);
        StartCoroutine(StageFourTutorial());
    }

    private IEnumerator SummonBoxes() {
        for (int i = 0; i < numberofBoxes; i++) {
            boxes[i] = Instantiate(boxPrefab, boxSpawner.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1f);
        }
        boxesSummoned = true;
    }

    private IEnumerator SummonHealingBoxes() {
        for (int i = 0; i < numberofHealingBoxes; i++) {
            Instantiate(boxHealPrefab, boxSpawner.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(1f);
        }
    }
    #endregion

#region Stage 4
    private void StageFourChecks() {
        if (moveGate) {
            gateWall.transform.position = Vector3.Lerp(gateWall.transform.position, gateWallTargetPos, 2f);
            if (gateWall.transform.position == gateWallTargetPos) {
                moveGate = false;
            }
        }
    }

    private IEnumerator StageFourTutorial() {
        stage = TutorialStage.Four;
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_FOUR_TEXT_1);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_FOUR_TEXT_2);
        checkpointPlatform.SetActive(true);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_FOUR_TEXT_3);
        checkpoint.setIsActive(true);
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.SetText(STAGE_FOUR_TEXT_4);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        mpc.SetText(STAGE_FOUR_TEXT_5);
        KeyForContinue();
        yield return new WaitUntil(() => expectingContinue == false);
        SetEndObjectsActive(true);
        moveGate = true;
        mpc.SetText(STAGE_FOUR_TEXT_6);
        yield return new WaitForSeconds(NextMessageInterval);
        mpc.HidePanel();
    }

    private void SetEndObjectsActive(bool active) {
        gateCollider.enabled = active;
        gateParticles.SetActive(active);
        cheatBattier.SetActive(active);
    }
#endregion
}
