﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRemoveOnDelay : MonoBehaviour {

	public float delayBeforeRemove = 2f;

	void Awake () {
		Destroy (gameObject, delayBeforeRemove);

	}
}
