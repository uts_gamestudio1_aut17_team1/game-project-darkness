﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCursor : MonoBehaviour
{
    public Texture2D cursorTexture;

    private int cursorSize = 30;

    private Vector3 cursorPosition;
    private Vector3 movement;
    private Vector3 newPosition;
    private Vector3 moveVelocity = Vector3.zero;

    private Vector3 oldMousePos;

	void Start ()
    {
        cursorPosition = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        Cursor.visible = false;
        oldMousePos = Input.mousePosition;
	}

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(cursorPosition.x, Screen.height - cursorPosition.y, cursorSize, cursorSize), cursorTexture);
    }

    void FixedUpdate()
    {
        float x = cursorPosition.x;
        float y = cursorPosition.y;

        if (Input.GetAxisRaw("RightJoystickX") > 0 || Input.GetAxis("Mouse X") > 0) {
            //stick moved to the right
            x += 5f;
            x -= ((oldMousePos - Input.mousePosition).x);
        } else if (Input.GetAxisRaw("RightJoystickX") < 0 || Input.GetAxis("Mouse X") < 0) {
            //stick moved to the left
            x -= 5f;
            x -= ((oldMousePos - Input.mousePosition).x);
        }

        if (Input.GetAxisRaw("RightJoystickY") > 0 || Input.GetAxis("Mouse Y") < 0) {
            //stick moved down
            y -= 5f;
            y -= ((oldMousePos - Input.mousePosition).y);
        } else if (Input.GetAxisRaw("RightJoystickY") < 0 || Input.GetAxis("Mouse Y") > 0) {
            //stick moved up
            y += 5f;
            y -= ((oldMousePos - Input.mousePosition).y);
        }

        movement.Set(x, y, transform.position.z);

        newPosition = transform.position + movement;
        newPosition.x = Mathf.Clamp(newPosition.x, 0, Screen.width - cursorSize);
        newPosition.y = Mathf.Clamp(newPosition.y, 0 + cursorSize, Screen.height);

        cursorPosition = Vector3.SmoothDamp(cursorPosition, newPosition, ref moveVelocity, 0.0001f);

		if (Input.GetAxis ("Mouse Y") > 0 || Input.GetAxis ("Mouse Y") < 0 || Input.GetAxis ("Mouse X") < 0 || Input.GetAxis ("Mouse X") > 0) {
			cursorPosition.Set (Input.mousePosition.x, Input.mousePosition.y, cursorPosition.z);
		}

        oldMousePos = Input.mousePosition;
    }

    public Vector3 getCursorPosition()
    {
        return cursorPosition;
    }
}
