﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialFallProtection : MonoBehaviour {

    public TutorialController tc;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("PlayerHead")) {
            tc.SoulFallen();
        }
        if (other.gameObject.CompareTag("Player")) {
            tc.BodyFallen();
        }
    }

}
