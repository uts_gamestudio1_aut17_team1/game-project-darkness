﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class AttackState : LogicalStateBehaviour {

    EntityController entityController;
    private float attackInterval;
    private float attackIntervalMultiplier = 0f;
    [Space(10)]
    public bool dealDamageEarlier = false;
    public float attackOffset;  // Should be a negative

    private float timer;
    private bool attackComplete;
    protected override void OnStateEntered() {
        entityController = Animator.gameObject.GetComponent<EntityController>();
        Animator.gameObject.GetComponent<NavMeshAgent>().isStopped = true;
        attackInterval = entityController.AttackInterval;
        attackIntervalMultiplier = Animator.GetCurrentAnimatorStateInfo(0).length / attackInterval;
        Debug.Log(Animator.GetCurrentAnimatorStateInfo(0).length);
        Animator.speed = attackIntervalMultiplier;

        if (dealDamageEarlier) {
            timer = attackInterval - attackOffset;
        }

        attackComplete = false;
    }

    
    protected override void OnStateUpdated() {
        if (dealDamageEarlier) {
            timer -= Time.deltaTime;
            if (timer <= 0) {
                if (!attackComplete) {
                    entityController.SoundController.PlayAttackSound();
                    if (Animator.GetBool("targetInAttackRange") && entityController.IsTargetInfront()) {
                        entityController.DamageTarget();
                        
                    }
                    attackComplete = true;
                }
            }
        }
        /*
            if (attackInterval <= 0) {
                timer -= Time.deltaTime;
                if (timer <= 0) {
                    if (Animator.GetBool("targetInAttackRange") && entityController.IsTargetInfront())
                    {
                        entityController.DamageTarget();
                    }
                    timer = attackInterval + attackOffset;
                }
            }
            */
    }
    

    protected override void OnStateExited()
    {
        Animator.speed = 1f;
        if (!dealDamageEarlier) {
            entityController.SoundController.PlayAttackSound();
            if (Animator.GetBool("targetInAttackRange") && entityController.IsTargetInfront()) {
                entityController.DamageTarget();
            }
        }
    }

}

