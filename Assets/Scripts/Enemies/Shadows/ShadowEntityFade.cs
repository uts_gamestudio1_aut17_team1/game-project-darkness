﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowEntityFade : MonoBehaviour {

    MeshRenderer mr;

    private void Awake() {
        mr = GetComponent<MeshRenderer>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("LightSource")) {
            mr.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("LightSource")) {
            mr.enabled = true;
        }
    }
}
