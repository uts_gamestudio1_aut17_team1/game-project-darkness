﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour {

	private GameObject currentCheckpoint;

	private GameObject player;
	private PlayerMovement playerMovement;
	private PlayerBodyController playerBodyController;
    private PlayerHeadController playerHeadController;
	private GameObject head;

	private Vector3 playerStartPosition;

	void Awake(){
		playerMovement = FindObjectOfType<PlayerMovement>();
		player = playerMovement.gameObject;
        playerBodyController = player.GetComponent<PlayerBodyController> ();
        playerHeadController = FindObjectOfType<PlayerHeadController>();
		playerStartPosition = player.transform.position;
	}

	void Update(){
//		if (Input.GetKeyDown (KeyCode.R)) {
//			RespawnPlayer ();
//		}
	}

	public void setCurrentCheckpoint(GameObject newCheckpoint){
		if (currentCheckpoint != null) {
			currentCheckpoint.GetComponent<CheckpointController> ().setIsActive (false);
		}
		currentCheckpoint = newCheckpoint;
		currentCheckpoint.GetComponent<CheckpointController> ().setIsActive (true);
	}

	public void RespawnPlayer(){
		Vector3 spawnPosition = playerStartPosition;
		if (currentCheckpoint != null) {
			spawnPosition = currentCheckpoint.GetComponent<CheckpointController> ().spawnLocation.transform.position;
		}

        playerBodyController.Respawn();
        playerHeadController.Respawn();
		player.transform.position = spawnPosition;
	}
}
