﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pauser : MonoBehaviour
{
    public GameObject pauseMenu;
    private bool paused;

	private PlayerBodyController playerBodyController;

	void Start ()
    {
        paused = false;
        Time.timeScale = 1;
		playerBodyController = GameObject.Find ("Player").GetComponent<PlayerBodyController> ();
    }

	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.P))
        {
			togglePause ();
        }
	}

	public void togglePause(){
		if (!paused)
		{
			Time.timeScale = 0;
			pauseMenu.SetActive(true);
		}
		else
		{
			Time.timeScale = 1;
			pauseMenu.SetActive(false);
		}
		paused = !paused;
		playerBodyController.enabled = !paused;

	}
}
