﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrapController : MonoBehaviour {
	public GameObject spike;
	public float activeDuration = 2f;
	public float inactiveDuration = 2f;

	public bool startActive = true;
	public bool randomize = false;
	private bool isActive;

	public float transitionRate = 0.01f;
	private float transitionScale;
	private bool isTransitioning;
	public Transform inactivePosition;
	public Transform activePosition;

	private TrapDamage trapDamage;

	private AudioSource audioSource;

	void Start () {
		audioSource = GetComponent<AudioSource> ();
		if(startActive){
			SetActive();
			transitionScale = 1f;
		}
		else{
			SetInactive();
			transitionScale = 1f;
		}
	}

	void Awake(){
		trapDamage = GetComponentInChildren<TrapDamage> ();
	}

	void Update(){
		if (isTransitioning) {
			if (isActive) {
				transitionScale += transitionRate;
				if (transitionScale > 1) {
					transitionScale = 1;
				}
			} else {
				transitionScale -= transitionRate;
				if (transitionScale < 0) {
					transitionScale = 0;
				}
			}

			setSpikePosition (transitionScale);

			if (transitionScale >= 1 || transitionScale <= 0) {
				isTransitioning = false;
				float duration;
				if(isActive){
					if(randomize){
						duration = Random.Range(activeDuration * .25f, activeDuration * 1.25f);
					} else {
						duration = activeDuration;
					}
					Invoke("SetInactive", duration);
				} else {
					if(randomize){
						duration = Random.Range(inactiveDuration * .25f, inactiveDuration * 1.25f);
					} else {
						duration = inactiveDuration;
					}
					Invoke("SetActive", duration);
					spike.SetActive (false);
				}
				trapDamage.setActive(isActive);

			}
			return;
		}
		spike.SetActive (isActive);

	}


	void SetActive(){
		isActive = true;
		isTransitioning = true;
		spike.SetActive (true);
		audioSource.Play ();
	}

	void SetInactive(){
		isActive = false;
		isTransitioning = true;
	}

	private void setSpikePosition(float positionTransition){
		Vector3 newSpikePosition = Vector3.Lerp (inactivePosition.position, activePosition.position, transitionScale);
		spike.transform.position = newSpikePosition;
	}

	public bool GetStatus(){
		return isActive;
	}

//	spike.gameObject.SetActive(false);
//	if(randomize){
//		duration = Random.Range(inactiveDuration * .25f, inactiveDuration * 1.25f);
//	}
//	Invoke("SetActive",duration);
//
//	spike.gameObject.SetActive(true);
//	if(randomize){
//		duration = Random.Range(activeDuration * .25f, activeDuration * 1.25f);
//	}
//	Invoke("SetInactive",duration);
}
