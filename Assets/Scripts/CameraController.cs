﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	/*
    public GameObject head;
	public GameObject body;

    private Vector3 offset;

	//z min, z max, x width
	public Vector3 cameraBounds = new Vector3(10, 7, 9);

	void Start ()
    {
        offset = transform.position - head.transform.position;
	}
	
	void LateUpdate ()
    {
        transform.position = head.transform.position + offset;
	}
	*/

	public float dampTime = 0.2f;                 // Approximate time for the camera to refocus.
	public float screenEdgeBuffer = 4f;           // Space between the top/bottom most target and the screen edge.
	public float minSize = 3.5f;                  // The smallest orthographic size the camera can be.
	public float maxSize = 13.5f;
	public Transform[] targets; // All the targets the camera needs to encompass. [HideInInspector] 


	private Camera camera;                        // Used for referencing the camera.
	private float zoomSpeed;                      // Reference speed for the smooth damping of the orthographic size.
	private Vector3 moveVelocity;                 // Reference velocity for the smooth damping of the position.
	private Vector3 desiredPosition;              // The position the camera is moving towards.

	public void start(){
		SetStartPositionAndSize ();
	}

	private void Awake ()
	{
		camera = GetComponentInChildren<Camera> ();
	}


	private void FixedUpdate ()
	{
		Move ();
		Zoom ();
	}


	private void Move ()
	{
		FindAveragePosition ();
		transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, dampTime);
	}


	private void FindAveragePosition ()
	{
		Vector3 averagePos = new Vector3 ();
		int numTargets = 0;
		for (int i = 0; i < targets.Length; i++)
		{
			if (!targets[i].gameObject.activeSelf)
				continue;

			// Add to the average and increment the number of targets in the average.
			averagePos += targets[i].position;
			numTargets++;
		}

		// If there are targets divide the sum of the positions by the number of them to find the average.
		if (numTargets > 0)
			averagePos /= numTargets;

		// Keep the same y value.
		averagePos.y = transform.position.y;

		// The desired position is the average position;
		desiredPosition = averagePos;
	}


	private void Zoom ()
	{
		// Find the required size based on the desired position and smoothly transition to that size.
		float requiredSize = FindRequiredSize();
		camera.orthographicSize = Mathf.SmoothDamp (camera.orthographicSize, requiredSize, ref zoomSpeed, dampTime);
	}


	private float FindRequiredSize ()
	{
		// Find the position the camera rig is moving towards in its local space.
		Vector3 desiredLocalPos = transform.InverseTransformPoint(desiredPosition);

		// Start the camera's size calculation at zero.
		float size = 0f;

		// Go through all the targets...
		for (int i = 0; i < targets.Length; i++)
		{
			// ... and if they aren't active continue on to the next target.
			if (!targets[i].gameObject.activeSelf)
				continue;

			// Otherwise, find the position of the target in the camera's local space.
			Vector3 targetLocalPos = transform.InverseTransformPoint(targets[i].position);

			// Find the position of the target from the desired position of the camera's local space.
			Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

			// Choose the largest out of the current size and the distance of the tank 'up' or 'down' from the camera.
			size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));

			// Choose the largest out of the current size and the calculated size based on the tank being to the left or right of the camera.
			size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / camera.aspect);
		}

		// Add the edge buffer to the size.
		size += screenEdgeBuffer;

		size = Mathf.Max (size, minSize);
		size = Mathf.Min (size, maxSize);

		return size;
	}


	public void SetStartPositionAndSize ()
	{
		// Find the desired position.
		FindAveragePosition ();

		// Set the camera's position to the desired position without damping.
		transform.position = desiredPosition;

		// Find and set the required size of the camera.
		camera.orthographicSize = FindRequiredSize ();
	}

	public Vector3 getCenterPosition(){
		if (targets[0] != null) {
			return targets[0].transform.position;
		} else {
			return transform.position;
		}
	}
}
