﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : BaseHealth{

    private Animator animator;
    private EntityController controller;

    void Awake()
    {
        animator = GetComponent<Animator>();
        controller = GetComponent<EntityController>();
    }

    public override void TakeDamage(int amount)
    {
        base.TakeDamage(amount);
        animator.SetTrigger("damage");
        controller.SoundController.PlayHurtSound();
    }

    protected override void Die() {
        StartCoroutine(GetComponent<EntityController>().Die());        
    }

}
