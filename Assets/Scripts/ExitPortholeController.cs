﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitPortholeController : MonoBehaviour {

	public int nextSceneIndex = 0;
    private AudioSource source;

    public Image whiteOutImage;

	private bool playerInPorthole;
	private GameObject player;
	private bool headInPorthole;
	private bool portholeActivated;

	private LevelCompleteScript levelCompleteScript;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        whiteOutImage.CrossFadeAlpha(0f, 1.0f, false);
		playerInPorthole = false;
		headInPorthole = false;
		portholeActivated = false;
		levelCompleteScript = GameObject.FindGameObjectWithTag ("HUD").GetComponent<LevelCompleteScript> ();
    }

	void Update(){
		if(playerInPorthole && headInPorthole && !portholeActivated){
			portholeActivated = true;
			activatePorthole ();
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("Player")){
			playerInPorthole = true;
			player = other.gameObject;
		}
		if(other.gameObject.CompareTag("PlayerHead")){
			headInPorthole = true;
		}
	}

	void OnTriggerExit(Collider other){
		if(other.gameObject.CompareTag("Player")){
			playerInPorthole = false;
		}
		if(other.gameObject.CompareTag("PlayerHead")){
			headInPorthole = false;
		}
	}

	private void activatePorthole(){
		Analytics.CustomEvent("Level Completed", new Dictionary<string, object> {
			{"Level", SceneManager.GetActiveScene().name },
			{"Gold", player.GetComponent<PlayerPoints>().getGold() },
			{"Body Deaths", player.GetComponent<PlayerBodyController>().DeathCount },
			{"Head Deaths", player.GetComponent<PlayerBodyController>().HeadController.DeathCount }
		});

		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		source.Play();
		whiteOutImage.CrossFadeAlpha(1f, 1.5f, false);

		Invoke("showLevelComplete", 1.6f);
	}

	private void showLevelComplete(){
		whiteOutImage.CrossFadeAlpha(0f, 1.0f, false);
		levelCompleteScript.showLevelCompleteMenu ();
	}


    void LoadNextLevel()
    {
        SceneManager.LoadScene(nextSceneIndex);
    }   

}
