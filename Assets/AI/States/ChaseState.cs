﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class ChaseState : LogicalStateBehaviour {
    private EntityController entityController;

    protected override void OnStateEntered() {
        entityController = Animator.gameObject.GetComponent<EntityController>();
        entityController.NavmeshAgent.isStopped = false;
    }

    protected override void OnStateUpdated() {
        //entityController.MoveToTarget();
        /*
        if (entityController.Target) {
            if (!Animator.GetBool("targetInAttackRange")) {
                entity.position = Vector3.MoveTowards(entity.position, entityController.Target.position, Time.deltaTime * entityController.MovementSpeed);
                entity.LookAt(entityController.Target);
            }
        }
        */
        if (entityController.Target) {
            if (!Animator.GetBool("targetInAttackRange")) {
                entityController.NavmeshAgent.SetDestination(entityController.Target.position);
            }
        }
    }

    protected override void OnStateExited() {
        entityController.NavmeshAgent.isStopped = true;
    }
}
