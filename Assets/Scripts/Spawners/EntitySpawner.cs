﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour {

    private class RespawnTimer {
        public Spawn spawn;
        public GameObject entity;
        public float respawnTime;
        public float timer = 0f;

        public RespawnTimer(Spawn spawn, GameObject spawnedEntity) {
            this.spawn = spawn;
            entity = spawnedEntity;
            respawnTime = spawn.respawnTimer;
        }
    }

    [System.Serializable]
    private struct Spawn {
        public GameObject entityPrefab;
        public int spawnAmount;
        public float respawnTimer;
        private int spawnedAmount;
    }

    [SerializeField]
    private float spawnRadius = 5f;
    [SerializeField]
    private bool isActive = true;
    [SerializeField]
    private List<Spawn> spawns = new List<Spawn>();
    private List<RespawnTimer> respawnTimers = new List<RespawnTimer>();

    public bool IsActive {
        get { return isActive; }
        set { isActive = value; }
    }


    void Awake() {
        foreach (Spawn s in spawns) {
            for (int i = 0; i < s.spawnAmount; i++) {
                GameObject entity = SpawnEntity(s.entityPrefab);
                if (s.respawnTimer != 0) {
                    CreateRespawnTimer(s, entity);
                }
            }
        }
    }

    private void Update() {
        if (isActive) {
            foreach (RespawnTimer rt in respawnTimers) {
                if (rt.entity == null) {
                    if ((rt.timer += Time.deltaTime) > rt.respawnTime) {
                        GameObject entity = SpawnEntity(rt.spawn.entityPrefab);
                        rt.entity = entity;
                        rt.timer = 0f;
                    }
                }
            }
        }
    }

    GameObject SpawnEntity(GameObject prefab) {
        Vector3 entityPos = transform.position + new Vector3(Random.Range(-spawnRadius, spawnRadius), 0.0f, Random.Range(-spawnRadius, spawnRadius));
        GameObject entity = Instantiate(prefab, entityPos, Quaternion.identity);
        return entity;
    }

    void CreateRespawnTimer(Spawn spawn, GameObject entity) {
        respawnTimers.Add(new RespawnTimer(spawn, entity));
    }
}
