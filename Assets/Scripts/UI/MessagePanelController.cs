﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessagePanelController : MonoBehaviour {

    private const string CONTINUE_TEXT = "Press {0} to continue...";
    private KeyCode continueButton = KeyCode.Space;

    public GameObject panel;
    public Text messageText;
    public GameObject continueText;
    private Text continueMessage;
    [Space(10)]
    public bool fadeOnTextChange = true;
    public float fadeTime = 0.25f;
    public float continueBlinkTime = 0.5f;

    private bool flashingMessage = false;

    public KeyCode ContinueButton {
        set { continueButton = value;
            continueMessage.text = string.Format(CONTINUE_TEXT, continueButton);
        }
    }

    private void Awake() {
        continueMessage = continueText.GetComponent<Text>();
    }

    public void SetText(string text) {
        StartCoroutine(FadeText(text));
    }

    public void FlashText(string text, float time) {
        StartCoroutine(Flash(text, time));
    }

    public void HidePanel() {
        panel.SetActive(false);
    }

    public void ShowPanel() {
        panel.SetActive(true);
    }

    public void ContinueTextActive(bool active) {
        continueText.SetActive(active);
        if (active) {
            StartCoroutine(BlinkText(continueMessage));
        }
    }

    private IEnumerator BlinkText(Text text) {
        yield return new WaitForSeconds(continueBlinkTime * 2);
        while (text.gameObject.activeInHierarchy) {
            text.CrossFadeAlpha(0f, continueBlinkTime, false);
            yield return new WaitForSeconds(continueBlinkTime);
            text.CrossFadeAlpha(1f, continueBlinkTime, false);
            yield return new WaitForSeconds(continueBlinkTime * 2);
        }
    }


    private IEnumerator FadeText(string text) {
        yield return new WaitUntil(() => !flashingMessage);
        messageText.CrossFadeAlpha(0f, fadeTime, false);
        yield return new WaitForSeconds(fadeTime);
        messageText.text = text;
        messageText.CrossFadeAlpha(1f, fadeTime, false);
    }

    private IEnumerator Flash(string text, float time) {
        bool isContinueTextActive = false;
        if (continueText.activeInHierarchy) {
            isContinueTextActive = true;
            ContinueTextActive(false);
        }
        flashingMessage = true;
        string originalText = messageText.text;
        messageText.CrossFadeAlpha(0f, fadeTime, false);
        yield return new WaitForSeconds(fadeTime);
        messageText.text = text;
        messageText.CrossFadeAlpha(1f, fadeTime, false);
        yield return new WaitForSeconds(time);
        messageText.CrossFadeAlpha(0f, fadeTime, false);
        yield return new WaitForSeconds(fadeTime);
        messageText.text = originalText;
        messageText.CrossFadeAlpha(1f, fadeTime, false);
        flashingMessage = false;
        if (isContinueTextActive) {
            ContinueTextActive(true);
        }
    }

}
