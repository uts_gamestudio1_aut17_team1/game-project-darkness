﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {
	private int sceneToLoad = 0;
    public void LoadByIndex(int sceneIndex)
    {
		Debug.Log ("Button clicked");
		sceneToLoad = sceneIndex;
		float fadeTime = GameObject.Find ("FadeManager").GetComponent<SceneFading> ().BeginFade (1);
		Debug.Log ("Fade out started");
		Invoke ("loadScene", fadeTime);
    }

	private void loadScene(){
		Debug.Log ("Swap scene");
		SceneManager.LoadScene(sceneToLoad);
	}
}
