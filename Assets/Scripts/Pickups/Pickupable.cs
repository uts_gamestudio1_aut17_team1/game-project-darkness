﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickupable : MonoBehaviour
{
    Rigidbody headRigidbody;

    private void Awake()
    {
        headRigidbody = GetComponent<Rigidbody>();
    }

    public void pickedUpBy(GameObject other)
    {
        transform.parent = other.transform;
        transform.position = other.transform.position;
        headRigidbody.isKinematic = true;
    }

    public void drop()
    {
        headRigidbody.isKinematic = false;
    }

    void FixedUpdate()
    { 

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            pickedUpBy(other.gameObject);
        }
    }
}
