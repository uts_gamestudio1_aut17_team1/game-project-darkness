﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour {

	public GameObject flame;
	public GameObject spawnLocation;
	private bool isActive;

    private EntitySpawner entitySpawner;
	private CheckpointManager checkpointManager;

	void Awake(){
		checkpointManager = FindObjectOfType<CheckpointManager> ();
        entitySpawner = GetComponent<EntitySpawner>();
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag("PlayerHead")) { //other.gameObject.CompareTag ("Player") || 
			if (!isActive) {
				checkpointManager.setCurrentCheckpoint (gameObject);
                if (entitySpawner) {
                    entitySpawner.IsActive = false;
                }
			}
		}
	}

	public void setIsActive(bool active){
		if (flame != null) {
			flame.SetActive (isActive = active);
			isActive = active;
			if (isActive) {
				GetComponent<AudioSource> ().Play ();
			} else {
				GetComponent<AudioSource> ().Stop ();
			}
		}
	}
}
