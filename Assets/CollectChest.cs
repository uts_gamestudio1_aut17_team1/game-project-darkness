﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectChest : MonoBehaviour {

    private const string ANIMATION_TRIGGER = "openChest";

    private bool isOpened = false;
    private bool isCollected = false;

    public int pointsValue = 100;
    public float collectionDelay = 0.5f;
    public GameObject[] collectionObjects;
    public AudioClip collectionSound;
    public AudioClip chestOpenSound;

	private ChestManager chestManager;

	void Start(){
		chestManager = GameObject.Find ("ChestManager").GetComponent<ChestManager> ();
		chestManager.registerChestToBeFound ();
	}

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Player") && 
            !collision.gameObject.GetComponent<PlayerBodyController>().CarryingHead) {
            if (!isOpened) {
                GetComponent<Animator>().SetTrigger(ANIMATION_TRIGGER);
                GetComponent<AudioSource>().PlayOneShot(chestOpenSound);
                isOpened = true;
                StartCoroutine(CollectPoints(collision.gameObject, collectionDelay));
            } else
                StartCoroutine(CollectPoints(collision.gameObject, 0f));
        }
    }

    private IEnumerator CollectPoints(GameObject player, float waitTime) {
        if (!isCollected) {
			isCollected = true;
            yield return new WaitForSeconds(waitTime);
			chestManager.chestFound ();
            GetComponent<AudioSource>().PlayOneShot(collectionSound);
            //player.GetComponent<PlayerPoints>().updatePoints(pointsValue);
            foreach (GameObject go in collectionObjects)
                go.SetActive(false);
        }
    }
}
