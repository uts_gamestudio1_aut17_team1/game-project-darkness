﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class HUDController : MonoBehaviour {

    [Header("Coin HUD Elements")]
    public GameObject coinDisplay;
    private Text coinText;
	private ChestManager chestManager;

    [Header("Health HUD Elements")]
    public GameObject healthDisplay;
    public Slider soulBar;
    public Slider healthBar;
    public GameObject lifeIconPrefab;
    public float healthSpacePadding = 5f;
    private GameObject[] lifeIcons;

    [Header("Damage Overlay")]
    public Image damageImage;
    private CanvasRenderer damageRenderer;

    private PlayerBodyHealth bodyHealth;
    private PlayerHeadHealth headHealth;

	[Header("Scene Navigation")]
	private LoadSceneOnClick loadSceneOnClick;
	private int currentSceneIndex;
	public int quitSceneIndex;
	public int nextSceneIndex;

    private void Awake() {
        coinText = coinDisplay.GetComponentInChildren<Text>();
        bodyHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBodyHealth>();
        headHealth = GameObject.FindGameObjectWithTag("PlayerHead").GetComponent<PlayerHeadHealth>();
		chestManager = GameObject.Find ("ChestManager").GetComponent<ChestManager> ();
		loadSceneOnClick = GetComponent<LoadSceneOnClick> ();
		currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;

        soulBar.maxValue = headHealth.MaxHealth;
        soulBar.value = headHealth.CurrentHealth;
        healthBar.maxValue = bodyHealth.MaxHealth;
        healthBar.value = bodyHealth.CurrentHealth;
        InitialiseLives();

        damageRenderer = damageImage.GetComponent<CanvasRenderer>();

		UpdateCoinValue (0);
    }

    private void Start() {
        SetImageTransparent(damageRenderer);

    }

    private void InitialiseLives() {
        lifeIcons = new GameObject[headHealth.Lives];
        RectTransform rt = lifeIconPrefab.GetComponent<RectTransform>();
        for (int i = 0; i < headHealth.Lives; i++) {
            Debug.Log("Width: " + rt.rect.width);
            lifeIcons[i] = Instantiate(lifeIconPrefab, healthDisplay.transform.position + new Vector3((rt.rect.width * i) + healthSpacePadding * i, 0, 0), lifeIconPrefab.transform.rotation, healthDisplay.transform);
        }
    }

    public void UpdateHealthUI() {
        soulBar.value = headHealth.CurrentHealth;
        healthBar.value = bodyHealth.CurrentHealth;
    }

    public void UpdateLifeUI() {
		int livesToShow = headHealth.Lives;
		for (int i = 0; i < lifeIcons.Length; i++) {
			if (livesToShow > 0) {
				lifeIcons [i].SetActive (true);
				livesToShow--;
			} else {
				lifeIcons [i].SetActive (false);
			}
		}
    }

    public void UpdateCoinValue(int value) {
		coinText.text = chestManager.getNumChestsFound () + " of " + chestManager.getNumChestsInLevel ();//value.ToString();
    }

    public void FlashDamageOverlay() {
        FlashImageOverlay(damageImage);
    }

    public void SetDamageOverlayOpaque() {
        SetImageOpaque(damageRenderer);
    }

    public void SetDamageOverlayTransparent() {
		SetImageTransparent(damageRenderer);
    }

    /*
     * Methods for fade in/outs and canvas renderers 
    */
    public void SetImageOpaque(CanvasRenderer cr) {
        cr.SetAlpha(1f);
    }

    public void SetImageTransparent(CanvasRenderer cr) {
        cr.SetAlpha(0f);
    }

    public void FlashImageOverlay(Image image) {
        StartCoroutine(FadeInThenOut(image));
    }

    IEnumerator FadeInThenOut(Image image) {
        Debug.Log("Fading Damage");
        image.CrossFadeAlpha(1f, 0.1f, false);
        yield return new WaitForSeconds(0.5f);
        image.CrossFadeAlpha(0f, 0.1f, false);
    }


	public void restartScene(){
		loadSceneOnClick.LoadByIndex (currentSceneIndex);
	}

	public void quitToMainMenu(){
		loadSceneOnClick.LoadByIndex (quitSceneIndex);
	}

	public void startNextLevel(){
		loadSceneOnClick.LoadByIndex (nextSceneIndex);
	}
}
