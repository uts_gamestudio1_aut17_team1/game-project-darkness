﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour {

    public string pointsType = "";
    public int pointsValue = 25;
    public AudioSource pickupSound;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerBodyController pCtrl = other.gameObject.GetComponent<PlayerBodyController>();
            if (pCtrl.CarryingHead)
            {
                PlayerPoints pPnts = other.gameObject.GetComponent<PlayerPoints>();
                if (pPnts)
                {
                    pPnts.updatePoints(pointsValue);

                    if (pickupSound != null)
                        pickupSound.Play();

                    Destroy(gameObject);
                }
            }
        }
    }
}
