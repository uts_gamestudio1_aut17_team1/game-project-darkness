﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggroRange : MonoBehaviour {
    EntityController parent;

    private void Awake() {
        parent = GetComponentInParent<EntityController>();
    }

    private void OnTriggerEnter(Collider other) {
        parent.AddAggroTarget(other);
    }

    private void OnTriggerExit(Collider other) {
        parent.RemoveAggroTarget(other);
    }
}
