﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class PlayerBodyController : MonoBehaviour {

    HUDController hud;

    Animator anim;

    [Header("Audio Settings")]
    public AudioClip attackSound;
    public AudioClip deathSound;
    public AudioClip deathThud;
    [Space(10)]

    private AudioSource source;
    public AudioClip spawnSound;

    [Header("Carry Object Settings")]
    private bool canCarry = true;
    public Transform carryPoint;
    public float carrySpawnOffset = 0f;
    public float pickupSmoothing = 2f;
    [Space(10)]

    private GameObject carriedObject = null;

    private PlayerHeadController playerHeadController;
    private PlayerMovement playerMovement;
	private Rigidbody playerRgidbody;
    private PlayerBodyHealth health;
    private bool isDead = false;
    private bool isOverPickupableObject = false;
    private GameObject pickupableObject;

    private Vector3 spawnPos;

    [Header("Object Throw Settings")]
    [SerializeField]
    GameObject targetIndicatorPrefab;
    GameObject targetIndicator;
	float throwSpeed = 1;
	float maxYVelocity = 10;
    [Space(10)]

    private bool canAttack = true;
    private float attackDelay = 1f;
    private float attackTimer = 0f;
    [Header("Attack Settings")]
    public GameObject weaponObject;
    [SerializeField]
    private int attackDamage = 25;
    private int deathCounter = 0;

    [Header("Particles")]
    public GameObject respawnParticles;

    public bool CanCarry {
        get { return canCarry; }
        set { canCarry = value; }
    }
    public bool Carrying {
        get { return carriedObject != null; }
    }

    public bool CarryingHead {
        get { return Carrying && carriedObject.CompareTag("PlayerHead"); }
    }

    public Vector3 CarryPointPosition {
        get { return carryPoint.position; }
    }

    public PlayerHeadController HeadController {
        get { return playerHeadController; }
    }

    public int DeathCount {
        get { return deathCounter; }
    }

    public bool CanAttack {
        get { return canAttack; }
        set { canAttack = value; }
    }

    public Vector3 SpawnPosition {
        get { return spawnPos; }
    }

    private void Awake() {
        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUDController>();
        spawnPos = transform.position;
        playerHeadController = GameObject.FindGameObjectWithTag("PlayerHead").GetComponent<PlayerHeadController>();
        health = GetComponent<PlayerBodyHealth>();
        playerMovement = GetComponent<PlayerMovement>();
		playerRgidbody = GetComponent<Rigidbody> ();

        //anim = transform.Find("Player_Character").GetComponent<Animator>();
        anim = GetComponentInChildren<Animator>();
        source = GetComponent<AudioSource>();

        //source.PlayOneShot(spawnSound);

        targetIndicator = Instantiate(targetIndicatorPrefab);
    }

    void Update() {
        if (carriedObject) {
            UpdateCarryObject();
        }
        CheckKeyInput();
        if (attackTimer > 0) {
            attackTimer -= Time.deltaTime;
        }
    }

    void UpdateCarryObject() {
        carriedObject.transform.position = Vector3.Lerp(carriedObject.transform.position, carryPoint.position, Time.deltaTime * pickupSmoothing);
    }

    void CheckKeyInput() {
        if (Input.GetButtonDown("X") || Input.GetMouseButtonDown(2)) {
            if (canCarry) {
                if (carriedObject != null) {
                    DropCarriedObject();
                } else if (isOverPickupableObject && pickupableObject != null) {
                    PickupObject(pickupableObject);
                }
            }
        }

        //may need work to get up/down working
        if (Input.GetButton("Y") || Input.GetMouseButton(1)) {
            targetIndicator.SetActive(true);
            targetIndicator.transform.position = playerMovement.MouseTargetPoint() + new Vector3(0, 0.1f, 0);
        }

        if (Input.GetButtonUp("Y") || Input.GetMouseButtonUp(1)) {
            ThrowPickedupObject();
        }

        if (Input.GetButtonDown("A") || Input.GetMouseButtonDown(0)) {
            if (canAttack) {
                if (carriedObject == null) Attack();
            }
        }
    }

    void DropCarriedObject() {

        carriedObject.GetComponent<Rigidbody>().isKinematic = false;
        carriedObject.transform.parent = null;

        if (carriedObject.CompareTag("PlayerHead")) {
            carriedObject.GetComponent<PlayerHeadController>().IsCarried = false;
            anim.SetBool("isCarryingHead", false);
            weaponObject.SetActive(true);
        }

        carriedObject = null;
    }

    void Attack() {
        if (attackTimer <= 0) {
            int layerMask = ~((1 << 9) | (1 << 10) | (1 << 11) | (1 << 12) | (1 << 13));
            anim.SetTrigger("Attack");
            source.PlayOneShot(attackSound, 0.08f);
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 1f, layerMask);
            Debug.Log("hits: " + hitColliders.Length);
            foreach (Collider hit in hitColliders) {
				
				Vector3 vectorToCollider = (hit.transform.position - transform.position);
				vectorToCollider.Normalize ();

				if (Vector3.Dot (vectorToCollider, transform.forward) > 0) {
					if (hit.transform.CompareTag ("Enemy") || hit.transform.CompareTag ("Destructable")) {
						hit.transform.gameObject.GetComponent<BaseHealth> ().TakeDamage (attackDamage);
					}
				}

            }
            attackTimer = attackDelay;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "PlayerHead") {
            isOverPickupableObject = true;
            pickupableObject = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "PlayerHead") {
            isOverPickupableObject = false;
            pickupableObject = null;
        }
    }

    public void ResetToSpawn() {
        transform.position = spawnPos;
    }

    public void PickupObject(GameObject o) {
        if (!isDead) {
            if (!carriedObject) {
                carriedObject = o;
                carriedObject.transform.parent = carryPoint;
                carriedObject.transform.position = carryPoint.position + transform.up * carrySpawnOffset;
                carriedObject.GetComponent<Rigidbody>().isKinematic = true;

                if (carriedObject.CompareTag("PlayerHead")) {
                    carriedObject.GetComponent<PlayerHeadController>().IsCarried = true;
                    anim.SetBool("isCarryingHead", true);
                    weaponObject.SetActive(false);
                }
            }
        }
    }

    public void DamageAnim() {
        anim.SetTrigger("TakeDamage");
    }

    public void Die() {
        deathCounter++;
        Analytics.CustomEvent("Player Body Died", new Dictionary<string, object> {
            {"Level", SceneManager.GetActiveScene().name},
            {"Position", transform.position }
        });
        if (health.IsDead)
        {
            hud.SetDamageOverlayOpaque();
            
            anim.SetTrigger("IsDead");
            source.PlayOneShot(deathThud);
            source.PlayOneShot(deathSound, 0.2f);
        }

        if (carriedObject != null) {
            DropCarriedObject();
			Debug.Log("Player droped object");
        }

        isDead = true;

		Debug.Log("Player movment deactivated");
		playerMovement.enabled = false;
		playerRgidbody.constraints = RigidbodyConstraints.FreezeAll;

        bool canRessurect = playerHeadController.RessurectBody();
        if (canRessurect) {
            Invoke("Respawn", 2f);
        }
    }

    public void Respawn() {
        GetComponent<Collider>().enabled = false;
        Debug.Log("Player respawned");

        //Debug.Log(transform.position);
        if (health.IsDead) {
            hud.SetDamageOverlayTransparent();
        }
        health.SetFullHealth();
        isDead = false;
        transform.position = playerHeadController.gameObject.transform.position + Vector3.up;
        PickupObject(playerHeadController.gameObject);
        GetComponent<Collider>().enabled = true;
        //Debug.Log(transform.position);
        respawnParticles.SetActive(true);
        StartCoroutine(DeactivateObject(respawnParticles, 1.5f));
		playerMovement.enabled = true;
		playerRgidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    private void ThrowPickedupObject() {
        targetIndicator.SetActive(false);
        Vector3 target = playerMovement.MouseTargetPoint();
        if (target == Vector3.zero || carriedObject == null) {
            return;
        }
        carriedObject.transform.parent = null;
        Rigidbody objectRB = carriedObject.GetComponent<Rigidbody>();
        if (carriedObject.CompareTag("PlayerHead")) {
            playerHeadController.IsCarried = false;
            anim.SetBool("isCarryingHead", false);
            weaponObject.SetActive(true);
        }
		carriedObject = null;
        objectRB.isKinematic = false;

		Vector3 targetDirection = target - transform.position;
		targetDirection.y = 0;
		float distance = Vector3.Distance (target, transform.position);
		float time = distance / throwSpeed;

		targetDirection.Normalize ();

		float vx = throwSpeed * targetDirection.x;
		float vz = throwSpeed * targetDirection.z;

		float vy = Mathf.Abs(Physics.gravity.y * time) / 2;
		if (vy > maxYVelocity) {
			vy = maxYVelocity;
			time = 2 * vy / Mathf.Abs (Physics.gravity.y);
			float tempThrowSpeed = distance / time;
			vx = tempThrowSpeed * targetDirection.x;
			vz = tempThrowSpeed * targetDirection.z;
		}

		objectRB.velocity = new Vector3(vx, vy, vz);
    }

    private IEnumerator DeactivateObject(GameObject obj, float time) {
        yield return new WaitForSeconds(time);
        obj.SetActive(false);
    }
}
