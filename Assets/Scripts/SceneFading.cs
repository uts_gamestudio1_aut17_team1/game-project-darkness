﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFading : MonoBehaviour {

	public Texture2D fadeOutTexture;
	public float fadeSpeed = 0.8f;

	private int drawDepth = -1000;
	private float alpha = 1.0f;
	private float fadeDir = -1;

	public bool showOnStartup = false;
	public bool fadeOnStartup = false;

	private float fadeSpeedStore;

	void Awake(){
		fadeSpeedStore = fadeSpeed;
		if (!showOnStartup) {
			alpha = 0f;
		}
	}

	void OnGUI(){
		alpha += fadeDir * fadeSpeed * Time.deltaTime;

		alpha = Mathf.Clamp01(alpha);

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeOutTexture);
	}

	public float BeginFade(int direction){
		fadeDir = direction;
		return fadeSpeed;
	}

	public void spotFade(float duration){
		float halfTime = duration / 2;
		fadeSpeed = 1 / halfTime;
		alpha = 0f;
		BeginFade (1);
		Invoke ("spotFadeIn", halfTime);
		Invoke ("resetFadeSpeed", duration);
	}

	private void spotFadeIn(){
		BeginFade (-1);
	}

	private void resetFadeSpeed(){
		fadeSpeed = fadeSpeedStore;
	}

	void OnLevelWasLoaded(){
		if (fadeOnStartup) {
			BeginFade (-1);
		}
	}

}
